// firebaseConfig.js

import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBBBJYs8LwjPSy7hJBSyuySfi5qk1FSyc4",
  authDomain: "aplicacion-1a105.firebaseapp.com",
  projectId: "aplicacion-1a105",
  storageBucket: "aplicacion-1a105.appspot.com",
  messagingSenderId: "779259086433",
  appId: "1:779259086433:web:bcd016c3c37e8a6e1b4c20",
  measurementId: "G-V3VKFLNDJE"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

export { app, auth };

